from __future__ import print_function

import collections
from Tkinter import *
import tkMessageBox
from tkFileDialog import askopenfilename, asksaveasfilename

class KKEdit(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        global appParent
        appParent = parent
        self.buildGUI(parent)
        self.addContextMenu(parent)
        
        # Needed class vars
        self.appEditor = None
        self.unsaved = False
        self.pressed = {}
        self.name = ""

    def buildGUI(self, parent):
        parent.geometry("800x600")
        parent.title("KKEdit")

    def addContextMenu(self, parent):
        menubar = Menu(parent)
        fmenu = Menu(menubar, tearoff=0)
        fmenu.add_command(label="New", command=self.newFile)
        fmenu.add_command(label="Open", command=self.openFile)
        fmenu.add_separator()
        fmenu.add_command(label='Save As..', command=self.saveFileAs)
        fmenu.add_command(label="Save", command=self.saveFile)
        fmenu.add_separator()
        fmenu.add_command(label='Close Open File', command=self.closeOpenFile)
        fmenu.add_command(label="Exit", command=self.closeApplication)
        
        hmenu = Menu(menubar, tearoff=0)
        hmenu.add_command(label='About...', command=self.aboutDialog)

        menubar.add_cascade(label="File", menu=fmenu)
        menubar.add_cascade(label="Help", menu=hmenu)

        parent.config(menu=menubar)

    def newFile(self):
        self.checkIfFileIsOpen(True)
        self.buildEditor(True)
        
    def openFile(self):
        self.fileName = askopenfilename(filetypes=[("All Files", "*.*")])
        try:
            file = open(self.fileName, "r")
        except FileNotFoundError:
            return
        except IOError:
            tkMessageBox.showerror("KKError", "Error: Error in trying to open the file requested. Please try again.")
        
        self.checkIfFileIsOpen(True)
        self.contents = file.read()
        file.close()
        self.buildEditor()
        n = self.fileName.split('/')
        self.name = n[-1]
        appParent.title("KKEdit | %s" % (self.name))
    
    def saveFile(self):
        if self.appEditor is None:
            tkMessageBox.showerror("KKEdit Error", "No files are currently open!")
        else:
            if self.appEditor:
                self.saveFileAs()
            else:
                fi = open(self.fileName, 'w')
                print(self.editor.get(1.0, 'end'), file=fi)
                fi.close()
                self.setTitle(False, False, False, True)
                self.unsaved = False
    
    def saveFileAs(self):
        if self.appEditor is None:
            tkMessageBox.showerror("KKEdit Error", "No files are currently open!")
        else:
            self.fileName = asksaveasfilename(filetypes=[("All Files", "*.*"), ("Text Files", "*.txt")], initialfile='Untitled.txt', defaultextension='.txt')
            fi = open(self.fileName, 'w')
            print(self.editor.get(1.0, 'end'), file=fi)
            fi.close()
            n = self.fileName.split('/')
            self.name = n[-1]
            appParent.title("KKEdit | %s*" % (self.name))
            self.setTitle(False, False, False, True)
            self.unsaved = False
            self.appEditor = False
    
    def closeOpenFile(self):
        if self.appEditor is None:
            tkMessageBox.showerror("KKEdit Error", "No files are currently open!")
        else:
            if self.unsaved:
                ans = tkMessageBox.askyesno("Warning", "Are you sure you want to exit? All unsaved changes will be lost!")
                if ans is True:
                    self.frame.pack_forget()
                    self.appEditor = None
                    self.contents = None
                    self.setTitle(False, False, True)
            else:
                self.frame.pack_forget()
                self.appEditor = None
                self.contents = None
                self.setTitle(False, False, True)

    def buildEditor(self, newfile = False):
        self.frame = Frame(appParent, bg='WHITE')
        self.editor = Text(self.frame, bd=0, wrap='word')
        sb = Scrollbar(self.frame)
        
        self.editor.pack(fill='both', expand=1, padx=15, side='left')
        sb.pack(side='right', fill='y')
        
        if newfile is True:
            self.setTitle(False, True, False)
        else:
            self.setTitle(False, False, False)
            
        try:
            self.contents
        except AttributeError:
            self.contents = ""
        
        if self.contents is None:
            self.contents = ""

        self.editor.focus_set()
        
        self.editor.insert('end', self.contents)
        
        self.editor.config(yscrollcommand=sb.set)
        sb.config(command=self.editor.yview)
        
        self.frame.pack(fill='both', expand=1)
        self.appEditor = newfile

        self.editor.bind("<Key>", self.setTitle)

    # Common methods
    '''
    def setBindings(self):
        for char in ['Control_L', 'a', 'c', 'z', 'x', 'v', 's']:
            self.editor.bind("<KeyPress-%s>" % (char), self._keyPressed)
            self.editor.bind("<KeyRelease-%s>" % (char), self._keyReleased)
            self.editor.bind("<Key>", self.setTitle)
    '''
    
    def setTitle(self, isKey = True, isNewFile = False, isCloseFile = False, isSaved = False):
        if isKey:
            self.markAsUnsaved()
        elif isNewFile:
            appParent.title("KKEdit | Untitled")
        elif isCloseFile:
            appParent.title("KKEdit")
        elif isSaved:
            self.markAsSaved()

    '''
    def _keyPressed(self, event):
        self.pressed[event.keycode] = True
        self.keyListener()

    def _keyReleased(self, event):
        self.pressed[event.keycode] = False
        self.keyListener()

    def keyListener(self):
        if self.pressed[17] and self.pressed[65]:
            self.editor.tag_add('sel', '1.0', 'end')
            self.editor.mark_set('insert', 1.0)
            self.editor.see('insert')
            return "break"
    '''
    
    def markAsSaved(self):
        title = appParent.title()
        d = collections.defaultdict(int)
        for c in title:
            d[c] += 1
        r = int("%d" % d["*"])
        if r > 0:
            ti = title[:-1]
            appParent.title(ti)
    
    def markAsUnsaved(self):
        title = appParent.title()
        d = collections.defaultdict(int)
        for c in title:
            d[c] += 1
        r = int("%d" % (d["*"]))
        if r == 0:
            if self.checkIfFileIsOpen():
                ti = "%s*" % (title)
                appParent.title(ti)
                self.unsaved = True
    
    def closeApplication(self):
        if self.unsaved:
            ans = tkMessageBox.askyesno("Warning", "Are you sure you want to exit? All unsaved changes will be lost!")
            if ans is True:
                appParent.quit
                exit()
        else:
            appParent.quit
            exit() # Disable while in Eclipse
    
    def checkIfFileIsOpen(self, close = False):
        if self.appEditor is not None:
            if close:
                self.closeOpenFile()
            else:
                return True
            
    def aboutDialog(self):
        window = Toplevel(self)
        window.title("About KKEdit")
        window.geometry("400x200")
        aboutText = StringVar()
        aboutText.set("""KKEdit | The New Python Based Text Editor
(c) 2013 Kaleb Klein.

Use of this program is presented and given on an 'AS IS' basis
Any use or modification of this program or contents is done at
Your own risk, and the creator is not responsible for anything
Beyond what's been presented in this state.

Licensed under the GNU LGPL license of open source software.

KKEdit Version: 0.2""")
        label = Label(window, textvariable=aboutText)
        label.pack(side="top", fill='both', padx=10, pady=10)
        
if __name__=='__main__':
    app = Tk()
    KKEdit(app).pack(side='top')
    mainloop()

KKEdit | The New Python Based Text Editor
(c) 2013 Kaleb Klein.

Use of this program is presented and given on an 'AS IS' basis
Any use or modification of this program or contents is done at
Your own risk, and the creator is not responsible for anything
Beyond what's been presented in this state.

Licensed under the GNU LGPL license of open source software.

############ Installation Requirements ############
Python 2.7.5 is required to run KKEdit. If you do not have it, you can head over to
http://www.python.org/ Click "Downloads" and download the latest version for your system

Check the Installation section for System specific installation instructions

############ Installation ############
On Windows, simply run the KKEdit.pyw file to open the editor without a console. KKEdit.py isn't needed for Windows systems.

At the moment, this doesn't work on Mac OSX At all, and hasn't been tested on Linux yet.
Sorry for this, and I hope to get this fixed very soon.

